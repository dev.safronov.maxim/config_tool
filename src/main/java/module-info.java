module config.tool {
    requires javafx.graphics;
    requires javafx.controls;
    requires javafx.fxml;
    requires jackson.annotations;
    requires jackson.dataformat.xml;

    opens app.frontend;
    opens app.backend;

    exports app.backend;
    exports app.frontend;
}