package app;

import app.backend.Backend;
import app.frontend.Frontend;
import javafx.application.Application;

import java.io.IOException;

public class Main {

    static Backend backend;

    public static void main(String[] args) {

        try {
            backend = new Backend();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Application.launch(Frontend.class, args);
    }
}
