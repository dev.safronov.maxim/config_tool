package app.backend;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;

import java.util.ArrayList;

public class parameters {

    @JacksonXmlElementWrapper(useWrapping = false)
    private ArrayList<Parameter> parameter;

    public parameters() {
    }

    public ArrayList<Parameter> getParameter() {
        return parameter;
    }

    @JsonIgnore
    public void addNewParameter() {
        Parameter newParam = new Parameter(parameter.size(), 0, 0, "", "Новый параметр " + parameter.size(), 0, 0, "", "", "", 0, 0, 0, 0, 0, 0, 0, "", "0", 0);
        this.parameter.add(newParam);
    }

    @JsonIgnore
    public void removeExistingParameter(parameters Parameter, int id) {
        Parameter.getParameter().remove(id);
        updateIndices(Parameter);
    }

    @JsonIgnore
    public void updateIndices(parameters Parameter) {
        Parameter.getParameter().forEach(p -> {
            p.setId(Parameter.getParameter().indexOf(p));
        });
    }
}
