package app.backend;

import app.frontend.ParametersTableClass;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;

import java.util.IllegalFormatException;

public class Parameter {

    public void setId(int id) {
        this.id = id;
    }

    public void setType(int type) {
        this.type = type;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setK_factor(double k_factor) {
        this.k_factor = k_factor;
    }

    public void setB_factor(double b_factor) {
        this.b_factor = b_factor;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public void setSmc(String smc) {
        this.smc = smc;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setNotification_value(double notification_value) {
        this.notification_value = notification_value;
    }

    public void setWarning_value(double warning_value) {
        this.warning_value = warning_value;
    }

    public void setMaximum_value(double maximum_value) {
        this.maximum_value = maximum_value;
    }

    public void setMinimum_value(double minimum_value) {
        this.minimum_value = minimum_value;
    }

    public void setDefault_value(double default_value) {
        this.default_value = default_value;
    }

    public void setPayload_position(int payload_position) {
        this.payload_position = payload_position;
    }

    public void setPayload_size(int payload_size) {
        this.payload_size = payload_size;
    }

    public void setMask(String mask) {
        this.mask = mask;
    }

    public void setShift(String shift) {
        this.shift = shift;
    }

    public void setDac(int dac) {
        this.dac = dac;
    }

    @JsonProperty("id")
    private int id;

    @JsonProperty("value")
    private double value;

    @JsonProperty("type")
    private int type;

    @JsonProperty("label")
    private String label;

    @JsonProperty("description")
    private String description;

    @JsonProperty("k_factor")
    private double k_factor;

    @JsonProperty("b_factor")
    private double b_factor;

    @JsonProperty("units")
    private String units;

    @JsonProperty("smc")
    private String smc;

    @JsonProperty("address")
    private String address;

    @JsonProperty("notification_value")
    private double notification_value;


    @JsonProperty("warning_value")
    private double warning_value;

    @JsonProperty("maximum_value")
    private double maximum_value;

    @JsonProperty("minimum_value")
    private double minimum_value;

    @JsonProperty("default_value")
    private double default_value;

    @JsonProperty("payload_position")
    private int payload_position;

    @JsonProperty("payload_size")
    private int payload_size;

    @JsonProperty("mask")
    private String mask;

    @JsonProperty("shift")
    private String shift;

    @JsonProperty("dac")
    private int dac;

    @JsonIgnore
    public Parameter(int id, double value, int type, String label, String description, double K_factor, double B_factor, String units, String smc, String address, double notification_value, double warning_value, double maximum_value, double minimum_value, double default_value, int payload_position, int payload_size, String mask, String shift, int dac) {
        this.id = id;
        this.value = value;
        this.type = type;
        this.label = label;
        this.description = description;
        this.k_factor = K_factor;
        this.b_factor = B_factor;
        this.units = units;
        this.smc = smc;
        this.address = address;
        this.notification_value = notification_value;
        this.warning_value = warning_value;
        this.maximum_value = maximum_value;
        this.minimum_value = minimum_value;
        this.default_value = default_value;
        this.payload_position = payload_position;
        this.payload_size = payload_size;
        this.mask = mask;
        this.shift = shift;
        this.dac = dac;
    }

    public Parameter() {
    }

    public synchronized int getId() {
        return id;
    }

    public synchronized int getType() {
        return type;
    }

    public synchronized String getLabel() {
        return label;
    }

    public synchronized String getDescription() {
        return description;
    }

    public synchronized double getK_factor() {
        return k_factor;
    }

    public synchronized double getB_factor() {
        return b_factor;
    }

    public synchronized String getUnits() {
        return units;
    }

    public synchronized String getSmc() {
        return smc;
    }

    public synchronized String getAddress() {
        return address;
    }

    public synchronized double getNotification_value() {
        return notification_value;
    }

    public synchronized double getWarning_value() {
        return warning_value;
    }

    public synchronized double getMaximum_value() {
        return maximum_value;
    }

    public synchronized double getMinimum_value() {
        return minimum_value;
    }

    public synchronized double getDefault_value() {
        return default_value;
    }

    public synchronized int getPayload_position() {
        return payload_position;
    }

    public synchronized String getMask() { return mask; }

    public synchronized String getShift() {
        return shift;
    }

    public synchronized int getDac() {
        return dac;
    }

    public synchronized int getPayload_size() {
        return payload_size;
    }

    public synchronized double getValue() {
        return value;
    }

    public synchronized void setValue(double value) {
        this.value = value;
    }

    public void update(ParametersTableClass parametersTableClass){
        switch (parametersTableClass.getId()){
            case 1:
                setValue(Double.parseDouble(parametersTableClass.getData()));
                break;
            case 2:
                setType(Integer.parseInt(parametersTableClass.getData()));
                break;
            case 3:
                setLabel(parametersTableClass.getData());
                break;
            case 4:
                setDescription(parametersTableClass.getData());
                break;
            case 5:
                setK_factor(Double.parseDouble(parametersTableClass.getData()));
                break;
            case 6:
                setB_factor(Double.parseDouble(parametersTableClass.getData()));
                break;
            case 7:
                setUnits(parametersTableClass.getData());
                break;
            case 8:
                setSmc(parametersTableClass.getData());
                break;
            case 9:
                setAddress(parametersTableClass.getData());
                break;
            case 10:
                setNotification_value(Double.parseDouble(parametersTableClass.getData()));
                break;
            case 11:
                setWarning_value(Double.parseDouble(parametersTableClass.getData()));
                break;
            case 12:
                setMaximum_value(Double.parseDouble(parametersTableClass.getData()));
                break;
            case 13:
                setMinimum_value(Double.parseDouble(parametersTableClass.getData()));
                break;
            case 14:
                setDefault_value(Double.parseDouble(parametersTableClass.getData()));
                break;
            case 15:
                setPayload_position(Integer.parseInt(parametersTableClass.getData()));
                break;
            case 16:
                setPayload_size(Integer.parseInt(parametersTableClass.getData()));
                break;
            case 17:
                setMask(Integer.toHexString(Integer.parseInt(parametersTableClass.getData(), 16)));
                break;
            case 18:
                setShift(parametersTableClass.getData());
                break;
            case 19:
                setDac(Integer.parseInt(parametersTableClass.getData()));
                break;
        }
    }
}
