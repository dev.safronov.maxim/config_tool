package app.backend;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import java.io.*;

public class Backend {

    public Backend() throws IOException {
//        Parameters = extractConfigData(parameters.class, "configParameters.xml");
//        assert Parameters != null;
//        Parameters.addNewParameter();
//        exportParametersToXML(Parameters, "configParameters" + "_new" + ".xml");
    }

    public static <T> T extractConfigData(Class<?> C, String fileName) {
        XmlMapper xmlMapper = null;
        String xml = null;
        try {
            File file = new File(fileName);
            xmlMapper = new XmlMapper();
            xml = inputStreamToString(new FileInputStream(file));
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            return (T) xmlMapper.readValue(xml, C);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void exportParametersToXML(parameters parameters, String fileName) {
        {
            XmlMapper xmlMapper = new XmlMapper();
            FileWriter file;
            try {
                file = new FileWriter(fileName);
                file.write("<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
                        "\n" +
                        "<!DOCTYPE parameters [\n" +
                        "        <!ELEMENT parameters (parameter)*>\n" +
                        "        <!ELEMENT parameter (id|value|type|label|description|k_factor|b_factor|units|smc|address|notification_value|warning_value|maximum_value|minimum_value|default_value|payload_position|payload_size|mask|shift|dac)*>\n" +
                        "        <!ELEMENT id (#PCDATA)>\n" +
                        "        <!ELEMENT value (#PCDATA)>\n" +
                        "        <!ELEMENT type (#PCDATA)>\n" +
                        "        <!ELEMENT label (#PCDATA)>\n" +
                        "        <!ELEMENT description (#PCDATA)>\n" +
                        "        <!ELEMENT k_factor (#PCDATA)>\n" +
                        "        <!ELEMENT b_factor (#PCDATA)>\n" +
                        "        <!ELEMENT units (#PCDATA)>\n" +
                        "        <!ELEMENT smc (#PCDATA)>\n" +
                        "        <!ELEMENT address (#PCDATA)>\n" +
                        "        <!ELEMENT notification_value (#PCDATA)>\n" +
                        "        <!ELEMENT warning_value (#PCDATA)>\n" +
                        "        <!ELEMENT maximum_value (#PCDATA)>\n" +
                        "        <!ELEMENT minimum_value (#PCDATA)>\n" +
                        "        <!ELEMENT default_value (#PCDATA)>\n" +
                        "        <!ELEMENT payload_position (#PCDATA)>\n" +
                        "        <!ELEMENT payload_size (#PCDATA)>\n" +
                        "        <!ELEMENT mask (#PCDATA)>\n" +
                        "        <!ELEMENT shift (#PCDATA)>\n" +
                        "        <!ELEMENT dac (#PCDATA)>\n" +
                        "        ]>\n\n"
                );
                String xml = xmlMapper.writeValueAsString(parameters);
                file.write(xml + "\n");
                file.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static String inputStreamToString(InputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        String line;
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        while ((line = br.readLine()) != null) {
            sb.append(line);
        }
        br.close();
        return sb.toString();
    }
}
