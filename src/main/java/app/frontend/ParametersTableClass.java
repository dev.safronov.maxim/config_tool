package app.frontend;

import javafx.beans.property.SimpleStringProperty;

public class ParametersTableClass {

    private SimpleStringProperty label;
    private SimpleStringProperty data;
    private final int id;

    ParametersTableClass(String name, String data, int id) {
        this.label = new SimpleStringProperty(name);
        this.data = new SimpleStringProperty(data);
        this.id = id;

    }

    public SimpleStringProperty labelProperty() {
        if (label == null) {
            label = new SimpleStringProperty(this, "name");
        }
        return label;
    }

    public SimpleStringProperty dataProperty() {
        if (data == null) {
            data = new SimpleStringProperty(this, "email");
        }
        return data;
    }

    public String getLabel() {
        return label.get();
    }

    public void setLabel(String fName) {
        label.set(fName);
    }

    public String getData() {
        return data.get();
    }

    public void setData(String fName) {
        data.set(fName);
    }

    public int getId() { return id; }
}
