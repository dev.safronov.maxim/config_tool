package app.frontend;

import app.backend.Backend;
import app.backend.parameters;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.TextFieldTreeTableCell;
import javafx.scene.input.ContextMenuEvent;
import javafx.scene.input.InputEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;

import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

public class Controller {

    private final ParameterTreeModel model = new ParameterTreeModel();
    String path;
    parameters Parameters;
    @FXML
    private ResourceBundle resources;
    @FXML
    private URL location;
    @FXML
    private TreeTableView<ParametersTableClass> tw;

    @FXML
    public void mouseClickedEvent() {

    }

    @FXML
    void openFileHandler(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        Stage stage = new Stage();
        stage.setTitle("Выберите файл");
        stage.initModality(Modality.WINDOW_MODAL);
        path = fileChooser.showOpenDialog(stage).getAbsolutePath();
        Parameters = Backend.extractConfigData(parameters.class, path);
        initTreeTableView(Parameters);
    }

    void initTreeTableView(parameters Parameters) {
        try {
            assert Parameters != null;
            TreeItem<ParametersTableClass> root = model.getParamsTree(Parameters);

            TreeTableColumn<ParametersTableClass, String> labelColumn = new TreeTableColumn<>("Наименование");
            labelColumn.setPrefWidth(300);
            labelColumn.setCellValueFactory(parametersTableClassStringCellDataFeatures -> parametersTableClassStringCellDataFeatures.getValue().getValue().labelProperty());

            TreeTableColumn<ParametersTableClass, String> dataColumn = new TreeTableColumn<>("Значение");
            dataColumn.setPrefWidth(150);
            dataColumn.setCellValueFactory(parametersTableClassStringCellDataFeatures -> parametersTableClassStringCellDataFeatures.getValue().getValue().dataProperty());
            dataColumn.setCellFactory(new Callback<TreeTableColumn<ParametersTableClass, String>, TreeTableCell<ParametersTableClass, String>>() {
                @Override
                public TreeTableCell<ParametersTableClass, String> call(TreeTableColumn<ParametersTableClass, String> parametersTableClassStringTreeTableColumn) {
                    return new TextFieldTreeTableCell<>();
                }
            });
            dataColumn.setCellFactory(TextFieldTreeTableCell.forTreeTableColumn());
            dataColumn.setOnEditCommit(new EventHandler<TreeTableColumn.CellEditEvent<ParametersTableClass, String>>() {
                @Override
                public void handle(TreeTableColumn.CellEditEvent<ParametersTableClass, String> parametersTableClassStringCellEditEvent) {
                    TreeItem<ParametersTableClass> currentEditingParameter = tw.getTreeItem(parametersTableClassStringCellEditEvent.getTreeTablePosition().getRow());
                    currentEditingParameter.getValue().setData(parametersTableClassStringCellEditEvent.getNewValue());
                    Parameters.getParameter().get(currentEditingParameter.getParent().getValue().getId()).update(currentEditingParameter.getValue());
                }
            });

            tw.addEventHandler(MouseEvent.MOUSE_RELEASED, new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouseEvent) {
                    if (mouseEvent.getButton() == MouseButton.SECONDARY){
                        ContextMenu contextMenu = new ContextMenu();
                        MenuItem addNewParameter = new MenuItem("Добавить новый параметр");
                        addNewParameter.setOnAction(new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent actionEvent) {
                                if (Parameters != null) {
                                    Parameters.addNewParameter();
                                    initTreeTableView(Parameters);
                                }
                            }
                        });
                        MenuItem removeParameter = new MenuItem("Удалить параметр");
                        removeParameter.setOnAction(new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent actionEvent) {
                                if (Parameters != null) {
                                    if (tw.getFocusModel().getFocusedCell().getTreeItem().getValue().getData() == "\n"){
                                        Alert removeParameterPrompt = new Alert(Alert.AlertType.CONFIRMATION);
                                        removeParameterPrompt.setTitle("Удаление параметра");
                                        removeParameterPrompt.setHeaderText(null);
                                        removeParameterPrompt.setContentText("Подтвердите удаление параметра");

                                        Optional<ButtonType> result = removeParameterPrompt.showAndWait();
                                        if (result.get() == ButtonType.OK) {
                                            Parameters.removeExistingParameter(Parameters, tw.getFocusModel().getFocusedCell().getTreeItem().getValue().getId());
                                            initTreeTableView(Parameters);
                                        }
                                    }
                                    else {
                                        Alert wrongActionError = new Alert(Alert.AlertType.ERROR);
                                        wrongActionError.setTitle("Ошибка");
                                        wrongActionError.setHeaderText(null);
                                        wrongActionError.setContentText("Выбранный элемент не является параметром");
                                        wrongActionError.showAndWait();
                                    }
                                }
                            }
                        });
                        contextMenu.getItems().addAll(addNewParameter, removeParameter);
                        tw.setOnContextMenuRequested(new EventHandler<ContextMenuEvent>() {
                            @Override
                            public void handle(ContextMenuEvent contextMenuEvent) {
                                contextMenu.show(tw, contextMenuEvent.getScreenX(), contextMenuEvent.getScreenY());
                            }
                        });
                    }
                }
            });

            dataColumn.setEditable(true);
            tw.setEditable(true);
            //Defining cell content
            tw.setRoot(root);
            tw.getColumns().setAll(labelColumn, dataColumn);
            tw.setPrefWidth(152);
            tw.setShowRoot(false);
        } catch (NullPointerException e) {
            System.out.println(e);
        }
    }

    @FXML
    void initialize() {

    }

    public void saveFileHandler() {
        if (path != null) {
            if (Parameters != null) {
                Alert saveFilePrompt = new Alert(Alert.AlertType.CONFIRMATION);
                saveFilePrompt.setTitle("Сохранение файла");
                saveFilePrompt.setHeaderText(null);
                saveFilePrompt.setContentText("Подтвердите сохранение файла");

                Optional<ButtonType> result = saveFilePrompt.showAndWait();
                if (result.get() == ButtonType.OK) {
                    Backend.exportParametersToXML(Parameters, path);
                    initTreeTableView(Parameters);
                    System.out.println("Done saving");
                }
            }
        }
    }

    public void addParameterHandler(MouseEvent mouseEvent) {

    }
}
