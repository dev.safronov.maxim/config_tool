package app.frontend;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;


public class Frontend extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/root.fxml"));
        primaryStage.setTitle("Конфигуратор дисплейного модуля");
        primaryStage.setScene(new Scene(root, 1280, 720));
        primaryStage.show();
    }
}
