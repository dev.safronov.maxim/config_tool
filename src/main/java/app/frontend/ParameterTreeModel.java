package app.frontend;

import app.backend.Parameter;
import app.backend.parameters;
import javafx.scene.control.TreeItem;

import java.util.*;

public class ParameterTreeModel {

    ArrayList<ParametersTableClass> values;

    public TreeItem<ParametersTableClass> getParamsTree(parameters parameters) {
        final TreeItem<ParametersTableClass> rootNode = new TreeItem<>(new ParametersTableClass("Параметры", "", -2));
        for (Parameter _params : parameters.getParameter()) {
            TreeItem<ParametersTableClass> item = new TreeItem<>(new ParametersTableClass(_params.getDescription(), "\n", _params.getId()));
            values = new ArrayList<>();
            values.add(new ParametersTableClass("Идентификатор: ", String.valueOf(_params.getId()), 0));
            values.add(new ParametersTableClass("Значение: ", String.valueOf(_params.getValue()), 1));
            values.add(new ParametersTableClass("Тип: ", String.valueOf(_params.getType()), 2));
            values.add(new ParametersTableClass("Обозначение: ", String.valueOf(_params.getLabel()),3));
            values.add(new ParametersTableClass("Описание: ", String.valueOf(_params.getDescription()), 4));
            values.add(new ParametersTableClass("Коэффициент К: ", String.valueOf(_params.getK_factor()), 5));
            values.add(new ParametersTableClass("Коэффициент В: ", String.valueOf(_params.getB_factor()), 6));
            values.add(new ParametersTableClass("Единицы измерения: ", String.valueOf(_params.getUnits()), 7));
            values.add(new ParametersTableClass("Код неисправности датчика: ", String.valueOf(_params.getSmc()), 8));
            values.add(new ParametersTableClass("Адрес в схеме тепловоза: ", String.valueOf(_params.getAddress()), 9));
            values.add(new ParametersTableClass("Предупредительное значение: ", String.valueOf(_params.getNotification_value()), 10));
            values.add(new ParametersTableClass("Аварийное значение: ", String.valueOf(_params.getWarning_value()), 11));
            values.add(new ParametersTableClass("Максимальное значение: ", String.valueOf(_params.getMaximum_value()), 12));
            values.add(new ParametersTableClass("Минимальное значение: ", String.valueOf(_params.getMinimum_value()), 13));
            values.add(new ParametersTableClass("Значение по умолчанию: ", String.valueOf(_params.getDefault_value()), 14));
            values.add(new ParametersTableClass("Номер младшего байта в посылке: ", String.valueOf(_params.getPayload_position()), 15));
            values.add(new ParametersTableClass("Количество байт информации: ", String.valueOf(_params.getPayload_size()), 16));
            values.add(new ParametersTableClass("Маска: ", String.valueOf(_params.getMask()), 17));
            values.add(new ParametersTableClass("Сдвиг: ", String.valueOf(_params.getShift()), 18));
            values.add(new ParametersTableClass("Число знаков после запятой: ", String.valueOf(_params.getDac()), 19));

            values.forEach(v -> {
                item.getChildren().add(new TreeItem<>(v));
            });
                rootNode.getChildren().add(item);
        }
        rootNode.setExpanded(true);
        return rootNode;
    }
}
